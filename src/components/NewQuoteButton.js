import React from 'react';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';

export default function NewQuoteButton({onNewQuote, isDisabled}) {
  return (
    <Button
      variant="primary"
      block onClick={onNewQuote}
      disabled={isDisabled}
      id="new-quote"
    >
      <FontAwesomeIcon icon={faPlusCircle} /> New Quote
    </Button>
  );
}
