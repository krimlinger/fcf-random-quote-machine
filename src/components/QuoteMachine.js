import React, { useState, useEffect, useRef } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';

import Quote from './Quote';
import TwitterButton from './TwitterButton';
import NewQuoteButton from './NewQuoteButton';

const QUOTES_URI = 'https://gist.githubusercontent.com/nasrulhazim/54b659e43b1035215cd0ba1d4577ee80/raw/e3c6895ce42069f0ee7e991229064f167fe8ccdc/quotes.json';
const RETRY_TIMEOUT = 5000;

function randomArrayElem(arr) {
  return arr[Math.floor(Math.random() * arr.length)];
}

export default function QuoteMachine() {
  const [quote, setQuote] = useState({quote: null, author: null});
  const [error, setError] = useState(null);
  const quotes = useRef([]);

  useEffect(() => {
    const fetchQuotes = async () => {
      try {
        let response = await fetch(QUOTES_URI);
        let result = await response.json();
        quotes.current = result.quotes;
        setQuote(randomArrayElem(result.quotes));
        setError(null);
      } catch (e) {
        setError(e);
        setTimeout(fetchQuotes, RETRY_TIMEOUT);
      }
    };
    fetchQuotes();
  }, []);

  return (
    <Container
      as="main"
      id="quote-box"
      className="p-3 bg-light rounded border border-primary"
    >
      <Row>
        <Col className="d-flex justify-content-center">
          <Quote quote={quote} />
        </Col>
      </Row>
      <Row>
        <Col className="d-flex justify-content-center">
          {error &&
          <Alert
            key={0} variant="danger" className="w-100" dismissible
            onClose={() => setError(null)}
          >
            An error happened while fetching quotes: <br />{String(error)}
          </Alert>
          }
        </Col>
      </Row>
      <Row className="justify-content-between align-items-center">
        <Col xs={4} className="d-flex align-self-stretch">
          <TwitterButton
            quote={quote}
          />
        </Col>
        <Col xs={8} className="d-flex align-self-stretch">
          <NewQuoteButton
            onNewQuote={() => setQuote(randomArrayElem(quotes.current))}
            isDisabled={!quotes.current.length}
          />
        </Col>
      </Row>
    </Container>
  );
}
