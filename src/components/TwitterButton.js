import React from 'react';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';


export default function TwitterButton({quote: {quote, author}}) {
  return (
    <Button
      href={`https://twitter.com/intent/tweet?text="${quote}" -${author}`}
      variant="secondary"
      block
      disabled={!(quote && author)}
      id="tweet-quote"
    >
      <FontAwesomeIcon icon={faTwitter} /> Tweet it
    </Button>
  );
}

