import React from 'react';
import Spinner from 'react-bootstrap/Spinner';

export default function Quote({quote: {quote, author}}) {
  return (
    <blockquote className="d-flex flex-column" id="quote-box">
      {(quote && author) ?
        (
          <>
            <p id="text">{quote}</p>
            <footer className="align-self-end" id="author">
              -{author}
            </footer>
          </>
        ):
        (
          <Spinner animation="border" role="status">
            <span className="sr-only">Fetching quote...</span>
          </Spinner>
        )}
    </blockquote>
  );
}
