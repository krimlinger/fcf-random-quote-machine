import React from 'react';
import './App.scss';

import QuoteMachine from './QuoteMachine';

export default function App() {
  return <QuoteMachine />;
}
